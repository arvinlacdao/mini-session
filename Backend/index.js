const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
const courseRoutes = require('./routes/course');
// Creates an express application
const app = express();

// Parsing - analyze (a string or text) into logical syntactic components
// Parses incoming requests with JSON payloads
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// Allows resource sharing from all origins
app.use(cors());

// Routes
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);

// Starts an HTTP server on a given port number
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`);
})

// Mongo DB Atlas
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:admin123@cluster0.7iowx.mongodb.net/minisession?retryWrites=true&w=majority', { 
	useNewUrlParser: true, 
	useUnifiedTopology: true,
	useFindAndModify: false
})

/*
// Local connection to mongoDB
mongoose.connect('mongodb://localhost:27017/restbooking', {
	useNewURLParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})
*/