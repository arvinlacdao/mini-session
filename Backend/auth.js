const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

// Create a JWT token
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// jwt.sign(payload, secretKey, options)
	return jwt.sign(data, secret, {});

}

// Verify authenticity of JWT token
// next passes the request to the next middleware/callback function
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	//console.log(token);

	if (typeof token !== 'undefined') {

		// Removes the "Bearer " from the received token
		// Used to get the token from the header data received
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			// next() passes the request to the next middleware/callback function in the route
			// next callback function = (req, res) => {}
			return (err) ? res.send({ auth: 'failed' }) : next();

		})

	} else {
		return res.send({ auth: 'failed' });
	}
}

// Decode the JWT token
module.exports.decode = (token) => {

	if (typeof token !== 'undefined') {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			// {complete: true} grabs both the request header and the payload
			return (err) ? null : jwt.decode(token, { complete: true }).payload;

		})

	} else {
		return null;
	}

}