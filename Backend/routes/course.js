const express = require('express');
// Creates a new router object
const router = express.Router();
const CourseController = require('../controllers/course');
const auth = require('../auth');

// Add course
router.post('/', auth.verify, (req, res) => {
    CourseController.add(req.body).then(result => res.send(result));
})

// Retrieve all courses
router.get('/', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses));
})

// Retrieve a specific course
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId;
    CourseController.get({ courseId }).then(course => res.send(course));
})

// Update a course
router.put('/', auth.verify, (req, res) => {
    CourseController.update(req.body).then(result => res.send(result));
})

// Soft delete/archive a course
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId;
    CourseController.archive({ courseId }).then(result => res.send(result));
})

// Hard delete
router.delete('/hard-delete/:courseId', auth.verify, (req,res) => {
	CourseController.delete(req.params.courseId).then(result => res.send(result))
})

// Exports the router object
module.exports = router;