const express = require('express');
// Creates a new router object
const router = express.Router();
const UserController = require('../controllers/user');
const auth = require('../auth');

// Check for duplicate emails
// router.verb(route, callback function)
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result));
})

// Register a user
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result));
})

// User login
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result));
})

// Retrieve user details
// router.get(route, middleware, callback)
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
    UserController.get({ userId: user.id }).then(user => res.send(user));
})

// Enroll a user
router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
    UserController.enroll(params).then(result => res.send(result));
})

// Exports the router object
module.exports = router;