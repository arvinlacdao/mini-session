const Course = require('../models/Course');

// Add course
module.exports.add = (params) => {

	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true;
	})

}

// Retrieve all courses
module.exports.getAll = () => {
	return Course.find({ isActive: true }).then(courses => courses);
}

// Retrieve a specific course
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course);
}

// Update a course
module.exports.update = (params) => {

	const updates = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	})

}

// Soft delete/archive a course
module.exports.archive = (params) => {

	const updates = { isActive: false };

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true;
	})

}

// Hard delete
module.exports.delete = (courseId) => {

	return Course.findByIdAndDelete(courseId).then((course, error) => {
		return (error) ? false : true;
		/*
		if(err){
			return false
		} else {
			return true
		}
		*/

	})

}