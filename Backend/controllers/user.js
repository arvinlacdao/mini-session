const bcrypt = require('bcrypt');
const User = require('../models/User');
const Course = require('../models/Course');
const auth = require('../auth');

// Check for duplicate emails
module.exports.emailExists = (params) => {

	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false;
	})

}

// Register a user
module.exports.register = (params) => {

	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		// 10 = salt/string of characters added to password before hashing
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user, err) => {
		return (err) ? false : true;
	})

}

// User login
module.exports.login = (params) => {

	return User.findOne({ email: params.email }).then(user => {

		if (user === null) { 
			return false;
		}

		// Compares the password recieved from the form and hashed password from the database
		// Returns true if values match
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password);

		// The mongoose toObject method converts the mongoose object into a plain javascript object
		// Used to show an object representation of the mongoose model
		// Mongoose objects will have access to .save() method while plain javascript won't

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) };
		} else {
			return false;
		}

		//console.log(user.save);
		//console.log(user.toObject().save)

	})
}

// Retrieve user details
module.exports.get = (params) => {

	return User.findById(params.userId).then(user => {

		// Re-assign the password to undefined so it won't be retrieved along with other user data
		user.password = undefined;
		return user;

	})

}

// Enroll a user
module.exports.enroll = (params) => {

	return User.findById(params.userId).then(user => {

		user.enrollments.push({ courseId: params.courseId });

		return user.save().then((user, err) => {

			return Course.findById(params.courseId).then(course => {

				course.enrollees.push({ userId: params.userId });

				return course.save().then((course, err) => {
					return (err) ? false : true;
				})

			})

		})

	})

}