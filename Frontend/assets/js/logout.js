// Clear your local storage
localStorage.clear();

// Redirect to the login page
window.location.replace('./login.html');