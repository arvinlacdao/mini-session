let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {

	// Prevents page redirection to avoid loss of input data whenever the registration process is not successful
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	// Validation to enable submit button when all fields are populated and both passwords match
	if ((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNumber.length === 11)) {

		// Check for duplicate email in database first
		fetch('http://localhost:4000/api/users/email-exists', {
		    method: 'POST',
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data);

			// If no duplicates found
		    if (data === false) {

		        fetch('http://localhost:4000/api/users', {
		            method: 'POST',
		            headers: {
		                'Content-Type': 'application/json'
		            },
		            body: JSON.stringify({
		            	firstName : firstName,
		            	lastName : lastName,
		            	email : email,
		            	password : password1,
		            	mobileNo : mobileNumber
		            })
		        })
		        .then(res => res.json())
		        .then(data => {

		        	console.log(data);

		            if(data === true){

		            	// Registration successful
		                alert("registered successfully");
		                // Redirect to login
		                window.location.replace("./login.html");

		            } else {

		                alert("Something went wrong.");

		            }

		        })

		    } else {

		    	alert("Duplicate email found.")

		    }

		})

	} else {

	    alert("Please check the information provided.");

	}
	
})