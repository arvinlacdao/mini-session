let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let cardFooter;

if(adminUser == "false" || !adminUser) {

    // If the user is a regular user, do not show the add course button
    modalButton.innerHTML = null;

} else {

    // Display the "Add Course" button if user is an admin
    modalButton.innerHTML =             
        `           
            <div class="col-md-2 offset-md-10">
                <a href="./addCourse.html" class="btn btn-block btn-primary"> 
                    Add Course 
                </a>
            </div>
        `

}

fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {
    
    // Check if all the courses were retrieved from the database
    console.log(data);

    // Creates a variable that will store the data to be rendered
	let courseData;

    // If the number of courses fetched is less than 1, display no courses available
    if(data.length < 1) {

        courseData = `<h2>No courses available</h2>`;

    } else {

        // Loop through the courses data and display each course as a card
        courseData = data.map(course => {
            
            // Print the ID of each course to confirm looping through the data
            console.log(course._id);

            if(adminUser == "false" || !adminUser) { 

            	// If the user is a regular user, render a "Select Course" button
            	// When the edit button is clicked, the user will be redirected to the "course.html" page which will contain the details of the course and an enroll button
            	// The course ID will be sent as a query string with the url to be used to retrieve the details of a course later on.
                cardFooter = 
                	`
                    	<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton"> 
                    		Select Course 
                    	</a>
                	`

            } else {

            	// If the user is an admin, render an "edit" and "delete" button instead
                 cardFooter = 
                 	`
                     	<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton"> 
                     		Edit 
                     	</a>
                        <a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block dangerButton"> 
                        	Disable Course 
                        </a>
                        <a href="./hardDelete.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block dangerButton"> 
                            Delete Course
                        </a>
                	`
            }
            
            // Display each element inside the courses collection by accessing its values
            return (
                `
                    <div class="col-md-6 my-3">
                        <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>${course.name}</h5>
                                <p class='card-text text-left'>
                                    ${course.description}
                                </p>
                                <p class='card-text text-right'>
                                   ₱ ${course.price}
                                </p>
                            </div>
                            <div class='card-footer'>
                                ${cardFooter}
                            </div>
                        </div>
                    </div>
                `
            )

        // Since the courses collection is an array, we can use the join method combine the elements into a single string value removing the commas in between.
        // We replaced the comma(,) with empty strings to remove the commas 
        }).join("")

    }

    let container = document.querySelector("#coursesContainer");

    // Assign the value of "courseData" as the courseContainer's content
    container.innerHTML = courseData;

})