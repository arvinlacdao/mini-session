let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {

	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	if (email == "" || password == "") {

		alert("please input your email and/or password");

	} else {

		fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

        	console.log(data);

            // Successful authentication will return a JWT via the response's accessToken property
            if (data.accessToken) {

                // Store JWT in localStorage
                localStorage.setItem('token', data.accessToken);

                // Send a fetch request to decode JWT and obtain user ID and role for storing in context
                fetch('http://localhost:4000/api/users/details', {
                    headers: {
                        'Authorization': `Bearer ${data.accessToken}`
                    } 
                })
                .then(res => res.json())
                .then(data => {

                    // Store the user's id and isAdmin properties in the localStorage
                    localStorage.setItem("id", data._id);
                    localStorage.setItem("isAdmin", data.isAdmin);
                    // Redirect to courses page
                    window.location.replace("./courses.html");

                })

            } else {

            	// Authentication failure
                alert("Authentication Failed.");

            }

        })

    }

})