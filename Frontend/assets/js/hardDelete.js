// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);
/*"file:///C:/Users/Lenovo/Desktop/Arvin/Projects/mini-session/Frontend/pages/courses.html?courseId=rho1273ur3u9r8u283r&text=hello"

let stringArray = params.split("=");
["file:///C:/Users/Lenovo/Desktop/Arvin/Projects/mini-session/Frontend/pages/courses.html?courseId" , "rho1273ur3u9r8u283r"]

stringArray[stringArray.length-1]*/

let courseId = params.get("courseId");

let token = localStorage.getItem('token');

fetch(`http://localhost:4000/api/courses/hard-delete/${courseId}`, {
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {

	console.log(data);

	if(data === true){

		// Delete course successful
	    // Redirect to courses page
	    window.location.replace("./courses.html");

	} else {

	    // Error in deleting a course
	    alert("something went wrong");

	}

})