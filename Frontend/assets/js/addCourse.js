let formSubmit = document.querySelector("#createCourse");
// Retrieve the JWT stored in our local storage for authentication
let token = localStorage.getItem('token');

formSubmit.addEventListener( "submit", (e) => {

	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	// Insert the course to our courses collection
	fetch('http://localhost:4000/api/courses', {
	    method: 'POST',
	    headers: {
	        'Content-Type': 'application/json',
	        'Authorization': `Bearer ${token}`
	    },
	    body: JSON.stringify({
	        name: courseName,
	        description: courseDescription,
	        price: coursePrice
	    })
	})
	.then(res => res.json())
	.then(data => {

    	console.log(data);

        if(data === true){

        	// Creation of new course successful
        	alert("Course successfully added!");
            // Redirect to courses page
            window.location.replace("./courses.html");

        } else {

            // Error in creating a course
            alert("Something went wrong.");

        }

	})

})