let token = localStorage.getItem("token");
// console.log(token);

let profileContainer = document.querySelector("#profileContainer");

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

	fetch('http://localhost:4000/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		profileContainer.innerHTML = 
			`
				<div class="col-md-12">
					<section class="jumbotron my-5">		
						<h3 class="text-center">First Name: ${data.firstName}</h3>
						<h3 class="text-center">Last Name: ${data.lastName}</h3>
						<h3 class="text-center">Email: ${data.email}</h3>
						<h3 class="text-center mt-5">Class History</h3>
						<div id="courses"></div> 

					</section>
				</div>
			`

		let courses = document.querySelector("#courses");

		data.enrollments.forEach(courseData => {

			console.log(courseData);

			fetch(`http://localhost:4000/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				courses.innerHTML += 
					`
						<div class="card">
							<div class="card-body">
								<h1 class="card-text">${data.name}</h1>
								<h3 class="card-text">${courseData.enrolledOn}</h3>
								<h3 class="card-text">${courseData.status}</h3>
							</div>
						</div>
					`

			})
		
		})

	})
}

/*==========
test codes
==========*/

/*let token = localStorage.getItem("token");
// console.log(token);

let profileContainer = document.querySelector("#profileContainer");

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.replace("./login.html");

} else {

	fetch('http://localhost:4000/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		let enrollmentData = data.enrollments.map(courseData => {

			console.log(courseData);

			let courseName;

			fetch(`http://localhost:4000/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				courseName = data.name;

			})

			return (
				`
					<tr>
						<td>${courseName}</td>
						<td>${courseData.enrolledOn}</td>
						<td>${courseData.status}</td>
					</tr>
				`
			)
		
		}).join("")

		profileContainer.innerHTML = 
			`
				<div class="col-md-12">
					<section class="jumbotron my-5">		
						<h3 class="text-center">First Name: ${data.firstName}</h3>
						<h3 class="text-center">Last Name: ${data.lastName}</h3>
						<h3 class="text-center">Email: ${data.email}</h3>
						<h3 class="text-center mt-5">Class History</h3>
						<table class="table">
							<thead>
								<tr>
									<th> Course ID </th>
									<th> Enrolled On </th>
									<th> Status </th>
								</tr>
							</thead>
							<tbody>
								${enrollmentData}
							</tbody>
						</table> 

					</section>
				</div>
			`

	})
}*/

