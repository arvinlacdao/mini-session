// "window.location.search" returns the query string part of the URL
console.log(window.location.search);

// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// To retrieve multiple query string values we can spread the data to set the key-value pairs of the object URLSearchParams
// console.log(...courseId);

// The "has" method checks if the "courseId" key exists in the URL query string
// The method returns true if the key exists
console.log(params.has('courseId'));

// The "get" method returns the value of the key passed in as an argument
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let courseName = document.querySelector("#courseName");
let courseDescription = document.querySelector("#courseDescription");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`http://localhost:4000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data);

	courseName.innerHTML = data.name;
	courseDescription.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-primary"> 
				Enroll 
			</button>
		`

	document.querySelector("#enrollButton").addEventListener("click", () => {
	    
	    // Insert the course to our courses collection
	    fetch('http://localhost:4000/api/users/enroll', {
	        method: 'POST',
	        headers: {
	            'Content-Type': 'application/json',
	            'Authorization': `Bearer ${token}`
	        },
	        body: JSON.stringify({
	        	courseId: courseId
	        })
	    })
	    .then(res => res.json())
	    .then(data => {

	        console.log(data);

	        if(data === true){

	            // Successful enrollment
	            alert('thank you for enrolling! see you!');
	            // Redirect to courses page
	            window.location.replace("./courses.html");

	        } else {

	            // Error in enrolling to a course
	            alert("something went wrong");

	        }

	    })

	})

})