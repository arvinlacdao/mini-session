/*
OOP - Object Oriented Programming vs Procedural
Class Based react vs Functional React

Java
React
*/

/*
OOP concepts
1. Encapsulation
2. Abstraction
3. Inheritance
4. Polymorphism
*/

// Encapsulation
// Putting code into certain blocks to allow/restrict access of information
// Separation of concerns
// userClass.js
private class User {
	name: String,
	email: String,
	getName: () => {
		return this.name
	},
	setName: (newName, token) => {
		if(token.isAdmin === true){
			return this.name = newName
		}
	}
	register: (name, email) => {

		let duplicate = Utilities.checkEmail(email);

		if(duplicate === false){
			fetch('http://localhost:4000/api/users', {
				body: JSON.stringify({
					this.name,
					this.email
				})
			})
		} else {
			console.log(error)
		}
		
	}
}

// utilities.js
class Utilities {
	checkEmail: (email) => {
		User.find(email)
	}
}

public class User {
	name: String,
	email: String
}

token {
	id: "fahufh2uihfi2"
	isAdmin: true
}

// registerForm.js
import User from "./userClass.js"

//public
let newUser = User({
	name: "arvin",
	email: "arvin@mail.com"
})

newUser.name = "john";
newUser.email = "john@mail.com";

//private
let newUser = User({
	name: "arvin",
	email: "arvin@mail.com"
})

//error
// newUser.name = "john";
// newUser.email = "john@mail.com";

newUser.setName("john", token);
newUser.setEmail("john", token);

// Abstraction
// Abstract/hide codes from users

// Inheritance
// Makes code reusable by inheriting properties and methods from other classes

class Admin extends User {
	isAdmin: true
	createCourse: () => {
		Course.save()
	}
}

class Manager extends Admin {

}

// index.js
let newUser = User({
	name: "arvin",
	email: "arvin@mail.com"
})

let admin = new newUser();
admin.isAdmin = true

// Polymorphism - many forms
// same names with different functions

class Manager extends Admin {
	delete: () => {
		Course.findByIdAndDelete()
	}
}

class TeamLead extends Admin {
	delete: () => {
		Course.findByIdAndUpdate({ isActive: false })
	}
}

let Ela = new Manager()
let Nes = new TeamLead()

Ela.delete()
Nes.delete()

function managerDelete()
function teamLeadDelete()